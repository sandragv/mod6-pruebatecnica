
const express = require('express');
const bodyParser = require('body-parser');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
    level: 'debug',
    format: combine(
        label({ label: 'main' }), timestamp(),
        myFormat
    ),
    defaultMeta: { service: 'user-service' },
    transports: [
        new transports.File({ filename: 'error.log', level: 'error' }),
        new transports.File({ filename: 'combined.log' }),
        new transports.Console()
    ]
});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let Events = [

    {
        nombre: "Beatles",
        tipo: "concierto",
        fecha: "fecha",
        lugar: "Londres",
        descripcion: "concierto en la azotea"
    }

]

//obtener los eventos
app.get('/events', async (req, res) => {
    const key = req.query['key'];

    if (key === undefined) {
        res.status(403).send();
        return;
    }

    res.send(Events);
});

//filtrar por tipo
app.get('/events', async (req, res) => {
    const key = req.query['key'];
    const tipo = req.query['tipo']

    if (key === undefined) {
        res.status(403).send();
        return;
    }
   
    if (tipo !== undefined) {
      const  filteredTipo = filteredTipo.filter(item => item['tipo'] === tipo);
    }

    if (filteredTipo.length === 0) {
        res.status(404).send('No hay datos');
        return;
    }

    res.send(filteredTipo);
});

//hacer post
app.post('/events', (req, res) => {
    const key = req.query['key'];

    if (key === undefined) {
        res.status(403).send();
        return;
    }

    let nombre = req.body['nombre'];
    let tipo = req.body['tipo'];
    let fecha = req.body['fecha'];
    let lugar = req.body['lugar'];

    let eventsData = {};

    eventsData['nombre'] = nombre;
    eventsData['tipo'] = tipo;
    eventsData['fecha'] = fecha;
    eventsData['lugar'] = lugar;

    for (let value of Object.values(eventsData)) {
        if (value === undefined || value.trim().length === 0) {
            res.status(400).send();
            return
        }
    }

    //me falta comprobar si se mete algún evento repetido (código 409)

    Events.push(eventsData);
    res.send();


});



app.listen(3333, function () {
    console.log('Example app listening on port 3333!');
});

